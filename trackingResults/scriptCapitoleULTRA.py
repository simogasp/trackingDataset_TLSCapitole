#!/usr/bin/env python
"""
Created on Thu Oct  8 18:13:16 2015

@author: sgaspari
"""
import argparse
import os.path
import subprocess
import shlex
import hashlib
import time

defaultTree = 'voctreeK10L5VLFEATfloat.tree'
defaultWeights = 'voctreeK10L5VLFEATfloat.weights'

#the executable
executable = './Darwin-i386-Release/openMVG_main_voctreeLocalizer'

# the directory containing the dataset
BASEPATH='/Users/sgaspari/dev/code/lib/data/ImageDataset_CapitoleTLS'
# the directory containing the OpenMVG reconstruction)
MVGPATH=BASEPATH+'/vlfeatUltra'
# the directory containing the shots)
TLSSHOT=BASEPATH+''

if __name__ == '__main__':    

    parser = argparse.ArgumentParser(description='Wrapper around voctreeLocalizer for Toulouse Capitole shots')
    parser.add_argument('shotnumber', nargs=1,
                        help='the shot number, eg 00041')
    parser.add_argument('mediafile', nargs='?', default='',
                        help='the media to localize (an image, a .txt, a json/bin, a video)')
    parser.add_argument('-a', '--algorithm',  default='AllResults', choices=['AllResults', 'FirstBest'],
                        help='The algorithm to use (default: %(default)s)')
    parser.add_argument('-p', '--preset',  default='ULTRA', choices=['LOW', 'MEDIUM', 'NORMAL', 'HIGH', 'ULTRA'],
                        help='The settings for the feature extractor (default: %(default)s)')
    parser.add_argument('-c', '--calibration', default='', 
                        help='Calibration file, if not provided it tries to use shotnumber.cal.txt unless --noCalibration is specified')
    parser.add_argument('--voctree', default=defaultTree, 
                        help='Filename for the vocabulary tree (default: %(default)s)')    
    parser.add_argument('--weights', default=defaultWeights, 
                        help='Filename for the vocabulary tree weights, if not specified is assumed the same as --voctree (default: %(default)s)')
    parser.add_argument('-e', '--export', default='', 
                        help='The name for the Alembic export file')
    parser.add_argument('-r', '--results', default='0', help='Number of images to retrieve in database (default: %(default)s)')
    parser.add_argument('--maxResults', default='10', help='Max number of succesful matched images to consider (default: %(default)s)')
    parser.add_argument('--refineIntrinsics', action='store_true', 
                        help='Enable/Disable camera intrinsics refinement')
    parser.add_argument('--globalBundle', action='store_true', 
                        help='If --refineIntrinsics is not provided, this option allows to run a final global budndle adjustment to refine the scene')
    parser.add_argument('--noCalibration', action='store_true', 
                        help='Do not use calibration')
    parser.add_argument('--dry', action='store_true', help='It just generates the command line to execute without launching it')
    args = parser.parse_args()

    # get an hash of hte current time (to be used to generate filenames)
    hash = hashlib.sha1()
    hash.update(str(time.time()).encode('utf-8'))
    
    # if a vocabulary tree has been passed but not the weights file
    if(not(args.voctree == defaultTree) and (args.weights == defaultWeights)):
        # set weights with the same filename
        args.weights = os.path.splitext(args.voctree)[0] + '.weights'
        
    
    calibfile = ''
    # if the calibration has been passed set the calibration file
    if args.calibration:
        calibfile = args.calibration
    #otherwise if no calib file has been passed and --noCalibration is not passed
    elif((not args.calibration) and (not args.noCalibration)):
        # it tries to use shotnumber.cal.txt
        calibfile = BASEPATH + '/' + args.shotnumber[0] +'.cal.txt'
        if not os.path.isfile(calibfile):
            print('\ndefault calibration file '+calibfile+' not found, running uncalibrated localization')
            args.noCalibration = True
        else:
            print('\nusing default calibration file '+calibfile)
    
    command = executable +'\n\t';
    
    outputName = ''
    #output name is used to build the output name of the alembic if not provided
    # as well as the name for the log file
    if(not args.export):
        # by default build the filename mixing some parameters
        outputName = 'capitole.'+ args.shotnumber[0]
        if args.mediafile:
            outputName += '.'+args.mediafile
        if args.noCalibration:
            outputName += '.uncal'
        else:
            outputName += '.cal'
        outputName += '.ULTRA.resized'
        outputName += '.algo'+args.algorithm
        outputName += '.r'+ str(args.results)
        if args.refineIntrinsics:
            outputName += '.refIntrinsics'
        outputName += '.'+os.path.splitext(args.voctree)[0]
        # add the hash of the time
        outputName += '.' + hash.hexdigest()[:10]
    else:
        #otherwise use the same filename passed at command line
        outputName = os.path.splitext(args.export)[0]
    
    # start building the command to execute
    command += ' --results ' + str(args.results) +'\n\t'
    command += ' --maxResults ' + str(args.maxResults) +'\n\t'
    command += ' --voctree ' + args.voctree +'\n\t'
    command += ' --weights ' + args.weights +'\n\t'
    command += ' --sfmdata ' + MVGPATH +'/reconstruction/sfm_data.bin' +'\n\t'
    command += ' --siftPath ' + MVGPATH +'/matches/' +'\n\t'
    command += ' --mediafile ' + TLSSHOT + '/'+ args.shotnumber[0] + '/' + args.mediafile +'\n\t'
    command += ' --algorithm ' + args.algorithm +'\n\t'
    if not args.noCalibration:
        command += ' --calibration ' + calibfile +'\n\t'
    if args.refineIntrinsics:
        command += ' --refineIntrinsics' +'\n\t'
    if args.globalBundle:
        command += ' --globalBundle ' +'\n\t'
    if not args.export:
        command += ' --export ' + outputName + '.abc' 
    else:
        command += ' --export ' + args.export 
#    command += ' > log_'+ outputName + '.txt 2>&1 &'

    print('\n'+command)
    print('\n log file: log_'+ outputName + '.txt')
#    print(shlex.split(command))
    if not args.dry:
      ptrack = subprocess.Popen(shlex.split(command.replace('\n\t','')), stdout=open('log_'+ outputName + '.txt', 'w'), stderr=subprocess.STDOUT)
      ptrack.wait()