# Videos

This folder contains 4 videos taken in the Place du Capitole while walking around (and at different time of the year w.r.t. the collection of images used for the 3D reconstruction).  The movement are quite random with frequent pans, for `00041.mp4` the path follows a circular motion following the 12 tips of the ["Croix Occitane"](https://en.wikipedia.org/wiki/Occitan_cross).

The camera used for these shots is the same as for the image collection, yet the intrinsics parameters (zoom) can be different. Note that there are no zooming in the videos, the intrinsics parameters are the same in all the 4 videos. 


## Calibration

The camera has been calibrated using the OpenCV sample program that comes with the library.  The intrinsics parameters are stocked in the `*.cal.txt` files of this directory in a format required by the localization script `openMVG_main_cameraLocalizer` of OpenMVG. The format is a quite simple plain text collecting one parameter value per row in this order:

```
 int #image width
 int #image height
 double #focal
 double #ppx principal point x-coord
 double #ppy principal point y-coord
 double #k0 first distortion parameter
 double #k1
 double #k2
```

## Convert to image sequence

The localization script `openMVG_main_cameraLocalizer` supports as input either a video file or a directory containing the images (more inputs are actually supported, please see the documentation of the script for more information). Since OpenCV is used to read the video, sometimes to avoid rebuilding OpenCV to support the needed codecs, it is easier to convert the video in a sequence of frames. This can be easily done with tools like `ffmpeg`:

```
ffmpeg -i input.mp4 img.%04d.png
```

The above command saves the frames of the input video `input.mp4` into a list of numbered images, i.e. `img.0001.png` for the first frame, `img.0002.png` for the second and so on. The `%04d` is used to add 0s as padding (up to 4) in the frame number. I strongly suggest this format with 4 figures and the dot after the text `img` as this pattern is easily recognized by Maya, if you need to import the result of your tracking and load the image planes associated to the animated camera.

## References

 - OpenMVG POPART fork: https://github.com/alicevision/openMVG
 - OpenCV libraries: http://opencv.org/
 - OpenCV calibration sample: https://github.com/Itseez/opencv/blob/master/samples/cpp/calibration.cpp 
 - OpenCV calibration doc: http://docs.opencv.org/3.1.0/d4/d94/tutorial_camera_calibration.html#gsc.tab=0